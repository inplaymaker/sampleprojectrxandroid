package com.samplerxandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.samplerxandroid.R;
import com.samplerxandroid.model.ProductDataModel;

import java.util.ArrayList;

public class ProductDataAdapter extends ArrayAdapter<ProductDataModel> {

    private final ArrayList<ProductDataModel> mProductDataCollection;
    private final int resource;
    private final Context mContext;
    private GetProductHolder mGetProductHolder;
    private final DisplayImageOptions options;
    private final ImageLoader imageLoader;

    public ProductDataAdapter(Context context, int resource, ArrayList<ProductDataModel> mProductDataCollection) {
        super(context, resource, mProductDataCollection);
        this.mContext = context;
        this.resource = resource;
        this.mProductDataCollection = mProductDataCollection;

        /*To display the image into imageview*/
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .defaultDisplayImageOptions(options)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
        imageLoader.init(config);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ProductDataModel col = mProductDataCollection.get(position);
        row = null;
        if(row == null){

            mGetProductHolder = new GetProductHolder();
            /*Inflating View*/
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row = inflater.inflate(resource, null, false);

            /*initializing variables of views from layout xml*/
            mGetProductHolder.Title = (TextView) row.findViewById(R.id.mProductTitle);
            mGetProductHolder.Price = (TextView) row.findViewById(R.id.mProductPrice);
            mGetProductHolder.ProductImage = (ImageView)row.findViewById(R.id.mProductImage);
            row.setTag(mGetProductHolder);

        }else{
            mGetProductHolder = (GetProductHolder) row.getTag();
        }

        //Setting the Text
        if(mProductDataCollection.size() > 0){
            mGetProductHolder.Title.setText(col.title);
            mGetProductHolder.Price.setText("£ "+col.price);
            imageLoader.displayImage(col.mainImage, mGetProductHolder.ProductImage, options);
        }

        return row;
    }

    /*Holder class to hold the list view*/
    public class GetProductHolder {
        TextView Title, Price;
        ImageView ProductImage;
    }
}
