package com.samplerxandroid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.samplerxandroid.model.ProductDataModel;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class CupboardDbHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "ProductData.db";
	private static final int DATABASE_VERSION = 1;

	public CupboardDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	static {
		//Register our models with Cupboard as usual
		cupboard().register(ProductDataModel.class);
	}

	//Creating database with their tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		cupboard().withDatabase(db).createTables();
	}

	//Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		cupboard().withDatabase(db).upgradeTables();
	}

}
