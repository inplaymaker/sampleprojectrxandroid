package com.samplerxandroid.util;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by sshara on 13-11-2015.
 */
public class Util {

    private ProgressDialog mDialog = null;
    private final Context mContext;

    public Util(Context mContext){
        this.mContext = mContext;
    }

    /*Showing ProgressDialog*/
    public void showProgressDialog(){
        if(mDialog == null) {
            mDialog = new ProgressDialog(mContext);
            mDialog.setCancelable(false);
            mDialog.setMessage("Loading...");
            mDialog.show();
        }
    }
    /*Hiding ProgressDialog*/
    public void hideProgressDialog(){
          if(mDialog != null){
              mDialog.hide();
              mDialog = null;
          }
    }

}
