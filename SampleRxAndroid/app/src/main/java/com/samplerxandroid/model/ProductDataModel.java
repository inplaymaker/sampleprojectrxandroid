package com.samplerxandroid.model;


import java.io.Serializable;

public class ProductDataModel implements Serializable {

    public Long _id; // for cupboard
    public final String price;
    public final String brand;
    public final String title;
    public final String mainImage;
    public final String longDescription;
    public String supplierProductId;
    public final String colour;
    public final String gender;

    public ProductDataModel() {
        this.price = "";
        this.mainImage = "";
        this.title = "";
        this.brand = "";
        this.longDescription = "";
        this.colour = "";
        this.gender = "";

    }

    public ProductDataModel(String supplierProductId, String title, String price, String brand, String mainImage, String longDescription, String colour, String gender) {
        this.price = price;
        this.supplierProductId = supplierProductId;
        this.mainImage = mainImage;
        this.title = title;
        this.brand = brand;
        this.longDescription = longDescription;
        this.colour = colour;
        this.gender = gender;
    }
}
