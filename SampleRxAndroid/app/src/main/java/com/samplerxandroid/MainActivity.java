package com.samplerxandroid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.samplerxandroid.adapter.ProductDataAdapter;
import com.samplerxandroid.database.CupboardDbHelper;
import com.samplerxandroid.model.ProductDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import nl.qbusict.cupboard.QueryResultIterable;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import com.samplerxandroid.util.Util;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class MainActivity extends AppCompatActivity {

    private ListView mListView;
    private ProductDataAdapter mProductDataAdapter;
    private SQLiteDatabase db;
    private CupboardDbHelper dbHelper;
    private JSONObject mResult;

    private ArrayList<ProductDataModel> mProductDataCollection = new ArrayList<>();
    private Button mButton;
    private Util mUtil;
    private TextView mError;
    private ArrayList<String> mSupplierId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*initializing variables of views from layout xml*/
        mListView = (ListView) findViewById(R.id.mListView);
        mButton = (Button) findViewById(R.id.mButton);
        mError  =(TextView) findViewById(R.id.mError);

        /*initializing object of Util class contains progressDialog*/
        mUtil = new Util(MainActivity.this);

        /*initializing object Databasehelper class*/
        dbHelper = new CupboardDbHelper(this);
        db = dbHelper.getWritableDatabase();

        /*Retrieving data from Database*/
        mProductDataCollection = getProductDataFromDataBase();

         /*If Database contains data then setting up adapter*/
        if(mProductDataCollection.size() > 0){
            mError.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);

            mProductDataAdapter =  new ProductDataAdapter(MainActivity.this, R.layout.productdatalistviewrow, mProductDataCollection);
            mListView.setAdapter(mProductDataAdapter);
            mProductDataAdapter.notifyDataSetChanged();
        }

        /*Registering button click Listener*/
        mButton.setOnClickListener(onClickListener);

        /*Registering ListView item click Listener*/
        mListView.setOnItemClickListener(onItemClickListener);
    }



    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mUtil.showProgressDialog(); // Showing progressDialog loader

            mError.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);

           /* Observable getProductData to make API call*/
            Observable<String> getProductData = Observable.create(new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber<? super String> subscriber) {
                    try {
                        //Calling getProductDataFromServer() to fetch the data from server
                        JSONObject data = getProductDataFromServer();
                        Thread.sleep(20000);
                        System.out.println("Data :  " + data);
                        subscriber.onNext(data.toString());
                        subscriber.onCompleted();
                    } catch (Exception e) {
                        subscriber.onError(e);
                    }
                }
            });

            /*Subscribing observable into main thread*/
            getProductData
                    .retry()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String result) {
                            Log.d("Result ", result);
                            setProductAdapter(result);
                            mUtil.hideProgressDialog();
                        }
                    });
        }
    };

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Start an another activity to show product details.
            Intent mIntent  = new Intent(MainActivity.this, ProductDetailActivity.class);
            mIntent.putExtra("mCollection", mProductDataCollection);
            mIntent.putExtra("mPosition", position);
            startActivity(mIntent);
            //finish();
        }
    };

    private JSONObject getProductDataFromServer() {
        //Volley JSON request to fetch the data from server
        RequestQueue mRequestQueue = Volley.newRequestQueue(getBaseContext());
        JsonObjectRequest mRequest = new JsonObjectRequest(Request.Method.POST, getResources().getString(R.string.url) , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Returns response
                Log.d("Response:", "" + response);
                mResult = response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Returns error
                Log.d("Error:", "" + error.toString());
                Toast.makeText(MainActivity.this, "Internal Server Error", Toast.LENGTH_LONG).show();
                mUtil.hideProgressDialog();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Add parameters to the POST method
                Map<String, String> parameters = new HashMap<>();
                parameters.put("n", "25");
                String filtersJson = "{\"genders\":[\"women\",\"unisex\"],\"priceRange\":{\"min\":\"0.00\",\"max\":\"250000.00\"}}";
                parameters.put("filters", filtersJson);
                return parameters;
            }

           @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                //Basic authentication to POST method
                HashMap<String, String> params = new HashMap<>();
                String credentials = getResources().getString(R.string.authentication);
                String auth = "Basic "+ Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        //API call again as timeout error occurs
        mRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(mRequest);

        return mResult;

         /*  RequestFuture<JSONObject> future = RequestFuture.newFuture();
         String url = "http://jsonplaceholder.typicode.com/posts";
         String url = "https://api-uat.bijouapp.com/the-edit/v2/60d684a6-00ca-455f-9c6e-730f14868890/products/search/";
         final Request.Priority priority = Request.Priority.IMMEDIATE;
         JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, future, future);
         MyVolley.getRequestQueue().add(req);
         return future.get();*/
    }

   /*Setting adapter and parsing API response*/
   private void setProductAdapter(String result) {
        JSONObject mJSONObject ;
        String price;
        String brand;
        String title;
        String mainImage;
        String longDescription;
        String supplierProductId;
        String colour;
        String gender;
        JSONArray mProductArray;

        if (result != null) {
            try {

                mProductDataCollection.clear();  /*clearing data from collection*/

                mJSONObject = new JSONObject(result);
                /*Parsing data*/
                mProductArray = mJSONObject.has("products") ? mJSONObject.getJSONArray("products") : null;
                if (mProductArray != null && mProductArray.length() >0) {
                    for(int i = 0 ; i < mProductArray.length() ; i++){
                        JSONObject mProductObject = mProductArray.getJSONObject(i);
                        title = mProductObject.has("title")? mProductObject.getString("title") : null;
                        price = mProductObject.has("price") ? mProductObject.getString("price") : null;
                        brand = mProductObject.has("brand") ? mProductObject.getString("brand") : null;
                        mainImage = mProductObject.has("mainImage")? mProductObject.getString("mainImage"):null;
                        longDescription = mProductObject.has("longDescription")? mProductObject.getString("longDescription"):null;
                        colour = mProductObject.has("colour")? mProductObject.getString("colour"):null;
                        gender = mProductObject.has("gender")? mProductObject.getString("gender"):null;
                        supplierProductId = mProductObject.has("supplierProductId")? mProductObject.getString("supplierProductId"):null;

                        /*Retrieving SupplierId from Database*/
                        mSupplierId = getSupplierIdFromDataBase();

                        /*Store data to database*/
                        if(!mSupplierId.contains(supplierProductId)) {
                           SaveProductDataToDataBase(supplierProductId,title, price, brand ,mainImage,longDescription, colour, gender);
                        }
                        mProductDataCollection.add(new ProductDataModel(supplierProductId,title, price, brand ,mainImage,longDescription, colour, gender));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }
        /*Setting up adapter*/
        if(mProductDataCollection != null && mProductDataCollection.size() > 0){
            mProductDataAdapter =  new ProductDataAdapter(MainActivity.this, R.layout.productdatalistviewrow, mProductDataCollection);
            mListView.setAdapter(mProductDataAdapter);
            mProductDataAdapter.notifyDataSetChanged();
        }
    }

    private void SaveProductDataToDataBase(String supplierProductId, String title, String price, String brand, String mainImage, String longDescription, String colour, String gender) {
       /*RxCupboard database inserting data to database*/
       ProductDataModel mProductDataBaseCollection = new ProductDataModel(supplierProductId,title, price, brand ,mainImage,longDescription, colour, gender);
       cupboard().withDatabase(db).put(mProductDataBaseCollection);
    }


    /*Database Operation for fetching data from database*/
    private ArrayList<ProductDataModel> getProductDataFromDataBase() {
        QueryResultIterable<ProductDataModel> itr = null;
        Cursor mCursor = cupboard().withDatabase(db).query(ProductDataModel.class).getCursor();

        ArrayList<ProductDataModel> mDataProductProductIdList =  new ArrayList<>();
        try {
            itr = cupboard().withCursor(mCursor).iterate(ProductDataModel.class);
            for (ProductDataModel mProductDataCollection : itr) {
                 /*Add data to the list*/
                   mDataProductProductIdList.add(mProductDataCollection);
            }
        } finally {
            if(itr != null)
               itr.close();
        }
        return mDataProductProductIdList;
    }


    /*Database Operation for fetching SupplierID from database*/
    private ArrayList<String> getSupplierIdFromDataBase() {
        QueryResultIterable<ProductDataModel> itr = null;
        Cursor mCursor = cupboard().withDatabase(db).query(ProductDataModel.class).getCursor();
        ArrayList<String> mSupplierIdList =  new ArrayList<>();
        try {
            itr = cupboard().withCursor(mCursor).iterate(ProductDataModel.class);
            for (ProductDataModel mProductDataCollection : itr) {
                 /*Add data to the list*/
                if(!mSupplierIdList.contains(mProductDataCollection.supplierProductId))
                    mSupplierIdList.add(mProductDataCollection.supplierProductId);

            }
        } finally {
            if(itr != null)
                itr.close();
        }
        return mSupplierIdList;
    }


}

