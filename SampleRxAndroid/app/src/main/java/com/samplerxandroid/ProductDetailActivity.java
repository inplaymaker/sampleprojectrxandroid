package com.samplerxandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.samplerxandroid.model.ProductDataModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ProductDetailActivity extends AppCompatActivity {


    private TextView mProductTitleTextView;
    private TextView mProductDescriptionTextView;
    private TextView mProductBrandTextView;

    private ImageView mProductImageView;

    DisplayImageOptions options;
    ImageLoader imageLoader;
    ArrayList<ProductDataModel> mCollection;
    int mPosition;
    private TextView mProductPriceTextView;
    private TextView mGenderTextView;
    private TextView mProductcolourTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productdescription);

        /*Getting data through  intent */

        mCollection = (ArrayList<ProductDataModel>) getIntent().getSerializableExtra("mCollection");
        mPosition = getIntent().getExtras().getInt("mPosition");

         /*Initilizing variables of views from layout xml*/
        mProductTitleTextView = (TextView) findViewById(R.id.mProductTitle);
        mProductBrandTextView = (TextView) findViewById(R.id.mProductBrand);
        mProductDescriptionTextView = (TextView) findViewById(R.id.mProductDescription);
        mProductImageView = (ImageView) findViewById(R.id.mProductImage);
        mProductPriceTextView = (TextView) findViewById(R.id.mProductPrice);
        mGenderTextView = (TextView) findViewById(R.id.mGender);
        mProductcolourTextView = (TextView) findViewById(R.id.mProductcolour);

      
        /*Setting data to their textview*/
        mProductTitleTextView.setText(mCollection.get(mPosition).title);
        mProductBrandTextView.setText(UpperCaseString(mCollection.get(mPosition).brand));
        mProductDescriptionTextView.setText(mCollection.get(mPosition).longDescription);
        mProductPriceTextView.setText("£ "+mCollection.get(mPosition).price);
        mGenderTextView.setText(UpperCaseString(mCollection.get(mPosition).gender));
        mProductcolourTextView.setText(UpperCaseString(mCollection.get(mPosition).colour));

         /*To display the image into imageview*/
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getBaseContext())
                .defaultDisplayImageOptions(options)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
        imageLoader.init(config);


        imageLoader.displayImage(mCollection.get(mPosition).mainImage, mProductImageView, options);


    }

    /*To convert String into upper case*/
    public String UpperCaseString(String mString){
        String mUpperCaseString = mString.substring(0, 1).toUpperCase() + mString.substring(1);
        return mUpperCaseString;

    }

}
